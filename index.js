"use strict";
 
const firstSlider = document.getElementById('first-slider');
const secondSlider = document.getElementById('second-slider');
const thirdSlider = document.getElementById('third-slider');
const fourthSlider = document.getElementById('fourth-slider');
const sliders = document.getElementsByClassName('slider');

const value_0 = document.getElementById('first-value');
const value_1 = document.getElementById('second-value');
const value_2 = document.getElementById('third-value');
const value_3 = document.getElementById('fourth-value');

// Render Function

function render() {
    value_0.textContent = firstSlider.value + '%';
    value_1.textContent = secondSlider.value + '%';
    value_2.textContent = thirdSlider.value + '%';
    value_3.textContent = fourthSlider.value + '%';
}

window.addEventListener("load", render);

// Update after changes

function update(e) {
    let valueArr = [];
    let sum = 0;
    for (let i = 0; i < sliders.length - 1; i++) {
        valueArr.push(parseInt(sliders[i].value));
    }
    sum = valueArr.reduce((a, b) => a + b);

    if (sum > 100) {
        e.target.nextElementSibling.style.color = 'red';

    } else {
        value_0.style.color = '#333333';
        value_1.style.color = '#333333';
        value_2.style.color = '#333333';
    }
    fourthSlider.value = (fourthSlider.max - sum);
    value_3.textContent = fourthSlider.value + '%';
    render();
}

for (let i = 0; i < sliders.length; i++) {
    sliders[i].addEventListener('input', update);
}